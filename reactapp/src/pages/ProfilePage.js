import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberTableHead from '../tables/MemberTableHead';



const ProfilePage = (props) => {

  console.log("Profile props getCurrentMember", props.getCurrentMember)

  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>Profile Page</h1>
        </Col>
      </Row>
      <Row className="d-flex">
        <Col md="8" className="">
        	<MemberTableHead/>
        </Col>
      </Row>
    </Container>
  );
}

export default ProfilePage;