import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import TeamsForm from '../forms/TeamsForm';
import TeamsTableHead from '../tables/TeamsTableHead';

const TasksPage = (props) => {

  console.log("TeamsPage props getCurrentMember", props.getCurrentMember)

  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>Teams Page</h1>
        </Col>
      </Row>
      <Row className="d-flex">
        <Col md="4" className="border">
        	<TeamsForm/>
        </Col>
        <Col md="8" className="">
        	<TeamsTableHead/>
        </Col>
      </Row>
    </Container>
  );
}

export default TasksPage;