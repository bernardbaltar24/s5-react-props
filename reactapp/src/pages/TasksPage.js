import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import TasksForm from '../forms/TasksForm';
import TasksTableHead from '../tables/TasksTableHead';

const TasksPage = (props) => {

  console.log("TasksPage props getCurrentMember", props.getCurrentMember)

  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>Tasks Page</h1>
        </Col>
      </Row>
      <Row className="d-flex">
        <Col md="4" className="border">
        	<TasksForm/>
        </Col>
        <Col md="8" className="">
        	<TasksTableHead/>
        </Col>
      </Row>
    </Container>
  );
}

export default TasksPage;