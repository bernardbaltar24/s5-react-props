import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import {Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';
import MemberForm from '../forms/MemberForm';
import MemberTableHead from '../tables/MemberTableHead';


const MembersPage = (props) => {

  console.log(!props.getCurrentMember.token)

  // if(!props.getCurrentMember.token ) return <Redirect to="/login" />

  console.log("MembersPage props getCurrentMember", props.getCurrentMember)

  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>{props.getCurrentMember.username} Page</h1>
        </Col>
      </Row>
      <Row className="d-flex">
        <Col md="4" className="border">
        	<MemberForm/>
        </Col>
        <Col md="8" className="">
        	<MemberTableHead/>
        </Col>
      </Row>
    </Container>
  );
}

export default MembersPage;