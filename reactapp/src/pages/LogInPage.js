import React from 'react';
import {Container , Row, Col} from 'reactstrap';
import LoginForm from '../forms/LoginForm'

const LoginPage = (props) => {
	return(
		<Container>
			<Row>
				<Col>
				  <h1>Login Page</h1>
				</Col>
			</Row>
			<Row>
				<Col>
					<LoginForm/>
				</Col>
			</Row>
		</Container>

		)
}

export default LoginPage;