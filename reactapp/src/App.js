import React, { useState } from 'react';
import {Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar' //kahit walang extension kasi js din sya.
import MembersPage from './pages/MembersPage';
import TeamsPage from './pages/TeamsPage';
import TasksPage from './pages/TasksPage';
import NotFoundPage from './pages/NotFoundPage';
import LogInPage from './pages/LogInPage';
import LandingPage from './pages/LandingPage';
import ProfilePage from './pages/ProfilePage';
import RegisterPage from './pages/RegisterPage';


const App = () => {

	const [appData, setAppData] = useState({
		token: localStorage.token,
		username: localStorage.username
	})

	const{token, username} = appData;
	console.log(username)
	// console.log(username)

	const getCurrentMember = () =>{
		return {username, token}
	}

	const Load = (props, page) => {

		//if for landing page		

		if(!token) return <Redirect to="/login" />

		if( page === "LogoutPage"){
			localStorage.clear()
			return window.location="/login"
		}

		switch(page){
			case "MembersPage": return <MembersPage {...props} getCurrentMember={getCurrentMember()} />
			case "ProfilePage": return <ProfilePage {...props} getCurrentMember={getCurrentMember()} />
			case "TeamsPage": return <TeamsPage {...props} getCurrentMember={getCurrentMember()} />
			case "TasksPage": return <TasksPage {...props} getCurrentMember={getCurrentMember()} />
			default: return <NotFoundPage/>
		}
	}
	

	return (
	<BrowserRouter>
		<AppNavbar token={token} username={username} getCurrentMember={getCurrentMember()} />
		<Switch>	
			<Route path="/members" render={ (props) => Load(props, "MembersPage") } />
			<Route path="/profile" render={ (props) => Load(props, "ProfilePage")} />
			<Route path="/teams" render={ (props) => Load(props, "TeamsPage")} />
			<Route path="/tasks" render={ (props) => Load(props, "TasksPage")} />
			<Route component= {LogInPage} path="/login"/>
			<Route path="/logout" render={(props)=>Load(props, "LogoutPage")} />
			<Route component= {RegisterPage} path="/register"/>
			<Route component= {LandingPage} exact path="/"/>
			<Route path="*" render={ (props) => Load(props, "NotFoundPage") } />
			
		</Switch>
	</BrowserRouter>
)}


export default App;