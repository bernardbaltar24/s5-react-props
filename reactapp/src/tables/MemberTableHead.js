import React from 'react';
import { Table } from 'reactstrap';
import MemberTableBody from '../rows/MemberTableBody';

const Example = (props) => {
  return (
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Email</th>
          <th>Team</th>
          <th>Position</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <MemberTableBody/>
      </tbody>
    </Table>
  );
}

export default Example;