import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const root = document.querySelector("#root")

const pageComponent = <App/>

ReactDOM.render(pageComponent, root);